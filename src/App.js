
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter,Route,Link} from 'react-router-dom'
import Answers from './components/answers/answers';

function App() {
  return (
    <BrowserRouter className="App">
      <Answers/>
      <div className='header'>
        
        <Link path='/answers' className='btn btn-primary' to="/Answers">Questions</Link>
        <Link path='/questions' className='btn btn-primary'>Add Question</Link>
      </div>
      
    </BrowserRouter>
  );
}

export default App;
